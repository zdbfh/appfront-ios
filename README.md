## Appfront - Grundidee ##

Wir haben uns eine innovative Applikation ausgedacht, welche auf der physischen Nähe von Menschen und deren Interessen aufbaut. Im Fokus soll dabei nicht der virtuelle Avatar sein (wie beispielsweise bei Facebook), sondern der Mensch dahinter. So sollen keine digitalen Interaktionen, sondern echte Konversationen entstehen. 

## Appfront - Ziel ##

Das Ziel unseres Projektes liegt darin eine sichere mobile Applikation mit Java-Backend zu entwickeln. Dabei liegt der Fokus auf den sicherheitskritischen Teilen. Die Use Cases sowie technologischen Herausforderungen werden dabei bewusst einfach gehalten zugunsten unseres Sicherheits-Fokus. Dies stellt auch die Grundlage für die spätere Diplomarbeit dar.

## Appfront - iOS Umsetzung ##

### In Scope ###

Hauptziele der Umsetzung im Rahmen iOS Programming:

1. Kommunikation zwischen App und Backend muss über eine Transport-Layer Verschlüsselung mit TLS basieren.
2. Backend musste so aufgesetzt werden, dass es mit der App kommunizieren und die verschiedenen Sicherheitsfunktionen nützen kann.
3. Das GUI repräsentiert die Abläufe und visuelle Umsetzung der Use Case.
4. Automatisierte Anfragen von Interessen bestimmter Personen sollte unmöglich sein. Bots und Skimming Versuche müssen verhindert werden.

### Out of Scope ###

App und Backend können zudem Nachrichten oder Teile von Nachrichten mit Public-Key Kryptographie für die jeweilige Gegenpartei verschlüsseln. Die Ver- und Entschlüsselung von Nachrichten wird zu einem späteren Zeitpunkt implementiert. Notwendige Verschlüsselte Informationen werden vorerst hartcodiert (Public-Key und verschlüsselte Client-Id).
Gewisse GUI Elemente haben im Moment noch keine Methoden hinterlegt. Der Fokus lag auf den Use-Cases Login und Registrierung via Backend.

## Use-Cases ##

### UC 1: Login ###
Der Benutzer muss sich der App gegenüber nicht authentifizieren. Der mit dem User assoziierte Account wird beim initialen Start der Applikation automatisch erstellt. Beim erneuten Login erledigt die App die Anmeldung für den Benutzer.

### UC 2: Wählen von Interessen ###

Der Benutzer kann auf dem Home-Screen ein Interesse auswählen. Die hierbei wählbaren Interessen kann er selbst spezifizieren oder er wählt ein aktives Interesse seiner Umgebung aus. Die derzeit aktiven Interessen werden ihm beim Login zur Verfügung gestellt.

![Use-Case 2 - UI](usecases/uc2_ui.png)

### UC 3: Sichtbarkeit ändern ###

#### UC 3.1: Sichtbarkeit aktivieren ####

Der Benutzer kann sich anderen Personen mit den gleichen Interessen sichtbar machen und gleichzeitig andere Personen mit den gleichen Interessen finden. Ausgangsbasis dafür sind die aus UC 2-1 gewählten Interessen.  

![Use-Case 3.1 - UI](usecases/uc31_ui.png)

#### UC 3.2: Sichtbarkeit deaktivieren ####

Der Benutzer möchte nicht mehr aktiv sein oder seine Interessen ändern. Um dies zu erreichen berührt er den „Button“ mit der Aufschrift „Deactivate“. Damit deaktiviert er seine Sichtbarkeit gegenüber anderen Personen, prüft nicht mehr auf gleiche Interessen in der unmittelbaren Umgebung und deaktiviert den Zugriff auf die Map.

![Use-Case 3.2 - UI](usecases/uc32_ui.png)

### UC 4: Benutzer mit gleichen Interessen auf Karte erkennen ###

Der Benutzer kann mit dem unteren Register auf eine „Map“ zugreifen. Auf dieser erkennt er anhand von Symbolen wo sich andere Personen befinden, die eines seiner Interessen teilen. Befinden sich mehre Personen am selben Ort, die seine Interessen/Leidenschaften teilen, erscheint ein Symbol, das einen Händedruck zeigt. Gibt es eine Person in der Umgebung, die zwei oder mehr Interessen mit dem Benutzer teilt, erscheint ein grösseres Symbol. 

![Use-Case 4 - UI](usecases/uc4_ui.png)

### UC 5: Gleichgesinnte in Reichweite ###

Zwei Benutzer mit gleichen Interessen sind nahe beieinander und erhalten eine Notifikation, sodass sie sich real finden.

### UC 6: Einstellungen ändern ####

Der Benutzer kann gewisse Standardeinstellungen der App ändern.

#### UC 6.1: Broadcast-Timer einstellen ####

Per Standardeinstellung ist der Benutzer nach Aktivierung der Sichtbarkeit 30 Minuten via Bluetooth seine Interessen am „Broadcasten“ und die anderer am Empfangen. Dies kann in den Settings angepasst werden. Der Benutzer kann hierbei wählen bis zu 4 Stunden sichtbar zu sein für andere.

#### UC 6.2: Reichweite einstellen #####

Per Standardeinstellung ist der Benutzer im Radius von einem Kilometer sichtbar auf der Karte. Diese Einstellung lässt sich beliebig verändern.

#### UC 6.3: Sichtbarkeit auf Map aktivieren/deaktivieren ####

Standarmässig ist das Häkchen „Sichtbarkeit auf Map“ nicht gesetzt und damit ist auch die Sichtbarkeit auf der Map deaktiviert. Hat der User beim UC 3-1 („Sichtbarkeit aktivieren“) jedoch der App bereits erlaubt seinen Standort für die Karte zu übermitteln, ist das Häkchen „Sichtbarkeit auf Map“ bereits gesetzt. Damit teilt die App dem Backend regelmässig den Standort des Benutzers, dessen Interessen und die Standorte jener anderen Benutzer mit gleichen Interesse mit. 
Diese Option kann der Benutzer im „Settings“-Menü deaktivieren.
Je länger die Option „Sichtbarkeit auf Map“ aktiviert ist, desto grösser ist der Radius auf der Karte, in dem der Benutzer andere erkennt und selbst erkannt werden kann. Der maximale Radius wurde dabei in den Settings festgelegt.