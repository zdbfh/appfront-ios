//
//  FirstViewController.swift
//  Appfront
//
//  Copyright © 2016 zd. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // Temporary public key, and encrypted client ID until encryption/decryption is possible.
    let publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjZoslQXQ1yqTL4IHZTiztZtyTeh0bovxhxD8QUe2AHkPB8y1MsPRV/H3klI9U2zmkGm4CQry9bLdTBcnh1J2xvPtPOi74E8VKHg+D/ShiyQRsXO+QobcnG+89L1a1v8hgeablUmisU3UvSyC4R+rRLFhtFHX0oBpuTxxEaNehfQMJS+0iQ1PGxnFoU0o45do3ZTYP+5EyXa5GV/H73aDdhhjeLtFbTT9qJPDc3nH5siR/gmmn04cwShjIcQNHcj3pg8E4gb2MXRtKkSRTPPpnFsvpatstM6E04F9OnwZEfX8B9VD1sWmHZDSs4x7LKSVhYH1abBZmdoyVuUB2eEKCwIDAQAB"
    let encryptedClientId = "MYV24jgVfLs3nuXNQTn7E7A3xtuCJVKUnyaRSXq3ja26T+6GnaNcfpZSQ0YbetvQzoVl/gjcMKJ8Uzu1KpuHTe/3sPaOnXfXs6OaJhEKeYx/mJ8/elC1RNamaXaemBepOGgII95YJASnWw7NFW5y3TXo+nbRDNA3bZCqVjSbU8sUv+ejNPDgEopdGhlNZBOlhl+T6JUo4+9MtsiIvxYf5BDlcPSjzVaIlKVcXZgVXMxnCENt3rPi6+tUSt2wZRVbdNEyuKAZHAqqZDtMcLbPvNJHPJy7mGwIrI4ndlWAKfzU5xUIYUvDT4n5NrdVU9z7QQOcHZtKr2N1YyRu3lruPQ=="
    
    var options:[String] = []
    var PlacementAnswer = 0
    
    @IBOutlet weak var ActivationButton: UIButton!
    @IBOutlet weak var Label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Registration data.
        var registerAuthDto = AuthenticationDTO()
        registerAuthDto.clientPublicKey = publicKey
        
        // Login data
        var loginAuthDto = AuthenticationDTO()
        loginAuthDto.clientId = encryptedClientId
        
        RegistrationControllerFacade().postRegister(registerAuthDto, completion: { registerResult in
            switch registerResult {
            case .Success(_):
                LoginControllerFacade().postLogin(loginAuthDto, completion: { loginResult in
                    switch loginResult {
                    case .Success(let loginData):
                        var values:[String] = ["Select your interest","Fussball","Joggen","Schwingen", "Tanzen"]
                        for activity in loginData {
                            values.append(activity.name)
                        }
                        self.setValues(values) // Here we need a refresh!
                        print(values)
                    case .Failure(let errorMessage):
                        print(errorMessage)
                    case .NetworkError(let error):
                        print(error)
                    }
                })
                self.setValues(["Select your interest","Fussball","Joggen","Schwingen", "Tanzen"])
            case .Failure(let errorMessage):
                print(errorMessage)
            case .NetworkError(let error):
                print(error)
            }
        })
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        setValues(["Select your interest","Fussball","Joggen","Schwingen"])
        
        Picker1.delegate = self
        Picker1.dataSource = self
        
        ActivationButton.enabled = false
    }
    
    func setValues(opts:[String]) {
        self.options = opts
        self.Picker1.reloadAllComponents()
        // Need to refresh the picker...
    }
    
    @IBOutlet weak var Picker1: UIPickerView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return options[row]
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return options.count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let itemSelected = options[row]
        ActivationButton.enabled = true
        Label.text = "You have chosen: \(itemSelected)"
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let destinationView:ActivateViewController = segue.destinationViewController as! ActivateViewController
            destinationView.OutputMessage = Label.text!
    }

}

