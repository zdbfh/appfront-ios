//
//  ActivityDTO.swift
//  DTO which contains data about an activity.
//
//  Copyright © 2016 zd. All rights reserved.
//

import Foundation

struct ActivityDTO {
    let name:String
}