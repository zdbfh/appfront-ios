//
//  MatchDTO.swift
//  DTO which contains data about a match.
//
//  Copyright © 2016 zd. All rights reserved.
//

import Foundation

struct MatchDTO {
    let reason:MatchType
    let latitude:String
    let longitude:String
}