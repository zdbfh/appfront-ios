//
//  InterestDTO.swift
//  DTO which contains data about an interest.
//
//  Copyright © 2016 zd. All rights reserved.
//

import Foundation

struct InterestDTO {
    let interestId:String
    let interests:[ActivityDTO]
}