//
//  MatchType.swift
//  Contains the known match types.
//
//  Copyright © 2016 zd. All rights reserved.
//

import Foundation

enum MatchType:Int {
    case BLUETOOTH = 0, MAP = 1
}