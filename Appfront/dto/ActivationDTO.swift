//
//  ActivationDTO.swift
//  DTO which contains data about an activation.
//
//  Copyright © 2016 zd. All rights reserved.
//

import Foundation

struct ActivationDTO {
    let authentication:AuthenticationDTO
    let interest:InterestDTO
    let visibilityType:MatchType
    let visibilityDuration:Int
}