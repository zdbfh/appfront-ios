//
//  AuthenticationDTO.swift
//  DTO which contains data about an authentication.
//
//  Copyright © 2016 zd. All rights reserved.
//

import Foundation

struct AuthenticationDTO {
    var clientId:String? = nil
    var clientRandom:String? = nil
    var clientPublicKey:String? = nil
}