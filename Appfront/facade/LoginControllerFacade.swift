//
//  LoginControllerFacade.swift
//  Controller facade for login.
//
//  Copyright © 2016 zd. All rights reserved.
//

import Foundation

class LoginControllerFacade:BaseFacade {

    //
    // Function to post login information to backend server.
    // Backend will then know that the client is still alive and deliver current activities back.
    //
    func postLogin(authenticationData:AuthenticationDTO, completion: (APIResult<[ActivityDTO]>) -> Void) {
        doRequest("login", method: "POST", body: serializeAuthentication(authenticationData)) { result in
            switch(result) {
            case .Success(let responseObject):
                let activityDto = self.deserializeActivity(responseObject as! [[String: AnyObject]])
                completion(.Success(activityDto))
            case .Failure(let errorMessage):
                completion(.Failure(errorMessage))
            case .NetworkError(let error):
                completion(.NetworkError(error))
            }
        }
    }
    
}