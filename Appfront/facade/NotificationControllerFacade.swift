//
//  NotificationControllerFacade.swift
//  Controller facade for notify and push notification.
//
//  Copyright © 2016 zd. All rights reserved.
//

import Foundation

class NotificationControllerFacade:BaseFacade {
    
    //
    // Function to post notify information to backend server.
    // Backend will then know that an appfront installation has detected another appfront installation.
    // If both installations have posted the notify request the installations will be notified.
    //
    func postNotify(activationData:ActivationDTO, otherClientInterestId:String) -> Void {
        // TODO : implement.
    }
    
    //
    // Function to post fetch notification information from the backend server.
    // As soon as both installations have posted a notify to the backend, the backend will notify the appfront installations.
    // In order to get informations about the match the appfront installations can request data about the match.
    //
    func postNotifications(activationData:ActivationDTO, completion: (APIResult<[MatchDTO]>) -> Void) {
        // TODO : implement.
    }
    
}