//
//  ActivationControllerFacade.swift
//  Controller facade for activation and deactivation of visibility.
//
//  Copyright © 2016 zd. All rights reserved.
//

import Foundation

class ActivationControllerFacade:BaseFacade {
    
    //
    // Function to post activation data to the backend.
    // Backend will then enable visibility of appfront installation.
    //
    func postActivate(activationData:ActivationDTO, completion: (APIResult<InterestDTO>) -> Void) {
        // TODO : finish implementation of activation
    }
   
    //
    // Function to post deactivation data to the backend.
    // Backend will then disable visbility of appfront installation and delete interest data.
    //
    func postDeactivate(activationData:ActivationDTO) -> Void {
        // TODO : finish implementation of deactivation
    }
    
}