//
//  APIResult.swift
//  Result of API request containing success data or error information.
//
//  Copyright © 2016 zd. All rights reserved.
//

import Foundation

enum APIResult<T> {
    case Success(T)
    case Failure(String)
    case NetworkError(NSError)
}