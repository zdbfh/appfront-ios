//
//  BaseFacade.swift
//  Base implementation of all facades containing basic backend communication and serialization/deserialization of data.
//
//  Copyright © 2016 zd. All rights reserved.
//

import Foundation

class BaseFacade {
    
    let baseUrl = "https://appfront.p3n.ch/rest/";
    
    let session:NSURLSession
    
    convenience init () {
        self.init(session:NSURLSession.sharedSession())
    }
    
    init(session:NSURLSession) {
        self.session = session
    }
    
    func getServiceUrl(serviceName:String) -> String {
        return baseUrl + serviceName
    }
    
    func doRequest(serviceName:String, method:String, body:AnyObject?, completion: (APIResult<Any>) -> Void) {
        let request = NSMutableURLRequest(URL: NSURL(string: getServiceUrl(serviceName))!)
        request.HTTPMethod = method
        if let HTTPBody = body {
            request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(HTTPBody, options:NSJSONWritingOptions(rawValue: 0))
            request.setValue("application/json", forHTTPHeaderField: "Content-type");
        }
        let task = session.dataTaskWithRequest(request, completionHandler: { (let data, let response, let error) in
            if let safeError = error {
                completion(.NetworkError(safeError))
            } else if let httpResponse = response as! NSHTTPURLResponse! {
                if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300) {
                    let responseObject: AnyObject?
                    if let safeData = data {
                        responseObject = try? NSJSONSerialization.JSONObjectWithData(safeData, options: NSJSONReadingOptions(rawValue: 0))
                    } else {
                        responseObject = nil
                    }
                    completion(.Success(responseObject))
                } else {
                    self.handleHttpError(httpResponse, completion: completion)
                }
            }
        })
        task.resume();
    }
    
    func handleHttpError<T>(httpResponse:NSHTTPURLResponse, completion:APIResult<T> -> Void) {
        if httpResponse.statusCode >= 400 && httpResponse.statusCode < 500 {
            completion(APIResult.Failure("An invalid request was promoted."))
        } else if httpResponse.statusCode >= 500 && httpResponse.statusCode < 600 {
            completion(APIResult.Failure("A backend problem occured."))
        } else {
            completion(APIResult.Failure("An unknown error occured."))
        }
    }
    
    // 
    // Function to serialize data from structure AuthenticationDTO to a JSON representation.
    //
    func serializeAuthentication(authenticationData:AuthenticationDTO) -> [String:AnyObject] {
        var serialized:[String:AnyObject] = [:]
        if (authenticationData.clientId != nil) {
            serialized["clientId"] = authenticationData.clientId
        }
        if (authenticationData.clientRandom != nil) {
            serialized["clientRandom"] = authenticationData.clientRandom
        }
        if (authenticationData.clientPublicKey != nil) {
            serialized["clientPublicKey"] = authenticationData.clientPublicKey
        }
        return serialized
    }
    
    //
    // Function to deserialize data from JSON representation to an instance of structure AuthenticationDTO.
    //
    func deserializeAuthentication(authenticationJson:[String:AnyObject]) -> AuthenticationDTO {
        return AuthenticationDTO(clientId: authenticationJson["clientId"] as? String,
            clientRandom: authenticationJson["clientRandom"] as? String,
            clientPublicKey: authenticationJson["clientPublicKey"] as? String)
    }
    
    //
    // Function to deserialize data from JSON representation to an instance of structure ActivityDTO.
    //
    func deserializeActivity(authenticationJson:[[String: AnyObject]]) -> [ActivityDTO] {
        var activities:[ActivityDTO] = []
        for activity in authenticationJson {
            activities.append(ActivityDTO(name: activity["name"] as! String))
        }
        return activities
    }

}