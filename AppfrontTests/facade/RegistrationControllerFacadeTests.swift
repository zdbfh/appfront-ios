//
//  RegistrationControllerFacadeTests.swift
//  appfront
//
//  Created by me on 19.06.16.
//  Copyright © 2016 zd. All rights reserved.
//

import XCTest
@testable import Appfront

class RegistrationControllerFacadeTests:XCTestCase {
    
    var ctrl:RegistrationControllerFacade!
    
    override func setUp() {
        ctrl=RegistrationControllerFacade()
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testRegister() {
        ctrl.postRegister(AuthenticationDTO(clientId:"a",clientRandom:"v",clientPublicKey:"test"), completion: { result in
            switch result {
            case .Success(let authData):
                XCTAssertTrue(authData.clientId == "a", "Client ID is 'a'")
            case .Failure(let errorMessage):
                XCTFail(errorMessage)
            case .NetworkError(_):
                XCTFail()
            }
        })
    }
    
}
